<?php

namespace arknova\encrypter;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{   
    public function init()
    {   
        $configFile = \Yii::getAlias('@app/config/encrypter.php');
        
        if (file_exists($configFile)) {
            $this->setComponents([
               'encrypter' => require($configFile),
            ]);
        }
        parent::init();
    }
    
    
    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            
            $this->controllerNamespace = 'arknova\encrypter\commands';
            $this->setAliases([
                '@arknova/encrypter' =>  dirname(__FILE__),
            ]);
        }
    }
}
