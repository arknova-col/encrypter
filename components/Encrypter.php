<?php

namespace arknova\encrypter\components;

use yii\base\Component;
use yii\base\InvalidConfigException;


class Encrypter extends Component
{    

    const AES128 = 'aes-128-cbc';
	const AES256 = 'aes-256-cbc';
    const IV_LENGTH = 16;
    
    /**
     * @var string
     */
    private $_globalPassword;
    /**
     * @var string
     */
    private $_iv;
    /**
     *
     * @var boolean
     */
    private $_use256BitesEncoding = false;
    /**
     *
     * @var boolean
     */
    private $_useBase64Encoding = false;
    
    /**
     * 
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->_globalPassword) {
            throw new InvalidConfigException('"' . get_class($this) . '::globalPassword" cannot be null.');
        }
        
        if (!$this->_iv) {
            throw new InvalidConfigException('"' . get_class($this) . '::iv" cannot be null.');
        }
    }
    
    /**
     * 
     * @param string $globalPassword the global password
     */
    public function setGlobalPassword($globalPassword)
    {
        $this->_globalPassword = $this->cleanString($globalPassword, 'globalPassword');
    }
    
    /**
     * 
     * @param string $iv the iv
     */
    public function setIv($iv)
    {
       $purifiedIV = $this->cleanString($iv, 'iv');
       
       if (strlen($purifiedIV) !== self::IV_LENGTH) {
           throw new InvalidConfigException('"' . get_class($this) . '::iv" should be exactly ' . self::IV_LENGTH . ' bytes long, ' . strlen($purifiedIV) . ' given.');
       }
       
       $this->_iv = $purifiedIV;
    }
    
    /**
     * 
     * @param boolean $use256BitesEncoding
     */
    public function setUse256BitesEncoding($use256BitesEncoding)
    {
        $this->checkBoolean($use256BitesEncoding, 'use256BitesEncoding');
        
        $this->_use256BitesEncoding = $use256BitesEncoding;
    }
    
    /**
     * 
     * @param boolean $useBase64Encoding
     */
    public function setUseBase64Encoding($useBase64Encoding)
    {
        $this->checkBoolean($useBase64Encoding, 'useBase64Encoding');
        
        $this->_useBase64Encoding = $useBase64Encoding;
    }
    
    /**
     * 
     * @param string $string the string to encrypt
     * @return string the encrypted string
     */
    public function encrypt($string)
    {
        $encryptedString = openssl_encrypt($string, $this->getCypherMethod(), $this->_globalPassword, true, $this->_iv);
        
        if ($this->_useBase64Encoding) {
            $encryptedString = base64_encode($encryptedString);
        }
        
        return $encryptedString;
    }
    
    /**
     * 
     * @param string $string the string to decrypt
     * @return string the decrypted string
     */
    public function decrypt($string)
    {
        $decodedString = $string;
        
        if ($this->_useBase64Encoding) {
            $decodedString = base64_decode($decodedString);
        }
        
        return openssl_decrypt($decodedString, $this->getCypherMethod(), $this->_globalPassword, true, $this->_iv);;
    }
    
    /**
     * 
     * @param string $value
     * @param string $propertyName
     * @return string trimmed value
     * @throws InvalidConfigException
     */
    private function cleanString($value, $propertyName)
    {
        if (!is_string($value)) {
            throw new InvalidConfigException('"' . get_class($this) . '::' . $propertyName . '" should be a string, "' . gettype($value) . '" given.');
        }
        
        $trimmedValue = trim($value);

        if (!strlen($trimmedValue) > 0) {
            throw new InvalidConfigException('"' . get_class($this) . '::' . $propertyName . '" length should be greater than 0.');
        }
        
        return $trimmedValue;
    }
    
    /**
     * 
     * @param boolean $value
     * @param string $propertyName
     * @throws InvalidConfigException
     */
    private function checkBoolean($value, $propertyName)
    {
        if (!is_bool($value)) {
            throw new InvalidConfigException('"' . get_class($this) . '::' . $propertyName . '" should be a boolean, "' . gettype($value) . '" given.');
        }
    }
    
    /**
     * Returns the cypher
     * 
     * @return string the cypher method
     */
    private function getCypherMethod()
    {
        if ($this->_use256BitesEncoding) {
            return self::AES256;
        }
        
        return self::AES128;
    }
}
