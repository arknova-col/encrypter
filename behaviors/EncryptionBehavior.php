<?php

namespace arknova\encrypter\behaviors;

use yii\db\ActiveRecord;
use yii\base\Event;
use yii\base\Behavior;
use arknova\encrypter\Encrypter;
use yii\base\InvalidConfigException;


class EncryptionBehavior extends Behavior
{
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'decryptAllAttributes',
            ActiveRecord::EVENT_BEFORE_INSERT => 'encryptAllAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'encryptAllAttributes',
            ActiveRecord::EVENT_AFTER_INSERT => 'decryptAllAttributes',
            ActiveRecord::EVENT_AFTER_UPDATE => 'decryptAllAttributes',
        ];
    }

    /**
     * Decrypts all attributes
     * 
     * @param Event $event
     */
    public function decryptAllAttributes(Event $event)
    {
        foreach ($this->attributes as $attribute) {
            $this->decryptValue($attribute);
        }
    }

    /**
     * Encrypts all attributes
     * 
     * @param Event $event
     */
    public function encryptAllAttributes(Event $event)
    {
        foreach ($this->attributes as $attribute) {
            $this->encryptValue($attribute);
        }
    }

    /**
     * Decrypts attribute.
     * 
     * @param string $attribute the attribute name
     */
    private function decryptValue($attribute)
    {
        
        $this->owner->$attribute = $this->getEncrypter()->decrypt($this->owner->$attribute);
        
    }

    /**
     * Encrypts attribute.
     * 
     * @param string $attribute the attribute name
     */
    private function encryptValue($attribute)
    {
        $this->owner->$attribute = $this->getEncrypter()->encrypt($this->owner->$attribute);
    }

    /**
     * Returns the Encrypter.
     * 
     * @return Encrypter
     * @throws InvalidConfigException
     */
    private function getEncrypter()
    {
        try {
            return \Yii::$app->encrypter;
        } catch (\Exception $exc) {
            throw new InvalidConfigException('Encrypter component not enabled.');
        }        
    }

}
