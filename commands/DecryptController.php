<?php

namespace arknova\encrypter\commands;

use yii\console\Controller;
use yii\helpers\Console;

class DecryptController extends Controller
{

    public function actionIndex()
    {
        $decryptedString = $this->getEncrypter()->decrypt($this->prompt("\nType here the string to decrypt:"));
        
        $this->stdout("\nDecrypted String:\n");
        $this->stdout($decryptedString, Console::FG_GREEN);
        $this->stdout("\n\n");
    }
    

    private function getEncrypter()
    {
        try {
            return $this->module->encrypter;
            
        } catch (\Exception $exc) {
            $this->stdout("The encrypter configuration not found.\n", Console::FG_RED);
        }
    }
    
}
