<?php

namespace arknova\encrypter\commands;

use yii\console\Controller;
use yii\helpers\Console;


class EncryptController extends Controller
{

    public function actionIndex()
    {
        $encryptedString = $this->getEncrypter()->encrypt($this->prompt("\nType here the string to encrypt:"));
        
        $this->stdout("\nEncrypted String:\n");
        $this->stdout($encryptedString, Console::FG_GREEN);
        $this->stdout("\n\n");
    }
    
    /**
     * 
     * @return \arknova\encrypter\components\Encrypter
     */
    private function getEncrypter()
    {
        try {
            return $this->module->encrypter;
            
        } catch (\Exception $exc) {
            $this->stdout("The encrypter configuration not found.\n", Console::FG_RED);
        }
    }
    
}
